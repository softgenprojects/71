import { Box, Flex, Heading, Input, Button, Text, VStack, HStack, Icon, Avatar, useColorModeValue, Tab, TabList, TabPanel, TabPanels, Tabs, SimpleGrid } from '@chakra-ui/react';
import { FiChevronRight, FiChevronLeft, FiHeart } from 'react-icons/fi';
import { BrandLogo } from '@components/BrandLogo';

const Home = () => {
  const bgColor = useColorModeValue('#000', '#000');
  const textColor = useColorModeValue('whiteAlpha.900', 'gray.800');

  return (
    <Box bg={bgColor} color={textColor} minH="100vh">
      <Flex justifyContent="space-between" alignItems="center" p="4">
        <Heading as="h1" size="lg">GPT Engineer</Heading>
        <HStack spacing="4">
          <Text>Projects</Text>
          <Avatar name="Marko Kraemer" src="https://i.pravatar.cc/300" />
          <Text>Marko Kraemer</Text>
        </HStack>
      </Flex>
      <VStack spacing="8" my="12">
        <Heading as="h2" size="xl">What do you want to build?</Heading>
        <Input placeholder="e.g. A todo app" size="lg" variant="filled" />
        <HStack overflowX="scroll" spacing="4" px="4">
          <Icon as={FiChevronLeft} w={6} h={6} />
          <Button variant="solid" colorScheme="teal">Todo</Button>
          <Button variant="solid" colorScheme="teal">Personal website</Button>
          <Button variant="solid" colorScheme="teal">Notes</Button>
          <Button variant="solid" colorScheme="teal">Band website</Button>
          <Button variant="solid" colorScheme="teal">Chat</Button>
          <Button variant="solid" colorScheme="teal">Dashboard</Button>
          <Icon as={FiChevronRight} w={6} h={6} />
        </HStack>
        <Button size="lg" colorScheme="red">Create</Button>
        <Tabs variant="enclosed">
          <TabList>
            <Tab>Featured</Tab>
            <Tab>Latest</Tab>
            <Tab>My Projects</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <SimpleGrid columns={{ base: 1, md: 3 }} spacing="4">
                {/* Project cards go here */}
              </SimpleGrid>
            </TabPanel>
            <TabPanel>
              {/* Content for Latest */}
            </TabPanel>
            <TabPanel>
              {/* Content for My Projects */}
            </TabPanel>
          </TabPanels>
        </Tabs>
      </VStack>
      <Flex justifyContent="center" alignItems="center" py="4">
        <Text mr="2">GPT Engineer is currently in alpha. The AI is expected to make mistakes</Text>
        <Icon as={FiHeart} color="red.500" />
        <Text ml="2">Made by Lovable</Text>
      </Flex>
    </Box>
  );
};

export default Home;